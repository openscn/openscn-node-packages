export class NotAvailableAction extends Error {
  constructor(action: string) {
    super(`NOTAVAIL: Action ${action} is not available`);
    this.name = 'NOTAVAIL'
  }
}

export class NotAvailableResource extends Error {
  constructor(resource: string) {
    super(`NOTAVAIL: Resource ${resource} is not available`);
    this.name = 'NOTAVAIL'
  }
}

export class RegistryError extends Error {
  constructor(error: Error) {
    super(error.message);
    this.name = error.name;
    this.stack = error.stack;
  }
}
