import fetch, { HeadersInit, BodyInit } from 'node-fetch';
import axios from 'axios';
import { RegistryError } from './errors';
import { TEST_RESOURCE } from './constants';

/**
 * 
 * @class EdgeComponentsClient 
 * It is the base class for clients of the edge components
 * registry service. It is used as an abstraction over the interactions a node
 * service may have with the edge components registry.
 * 
 */
export class EdgeComponentsClient {
  /**
   * @property {string} registryToken The token to use to authenticate against the registry service
   */
  private registryToken: string;
  
  /**
   * @property {string} registryUrl The url of the registry service
   */
  private registryUrl: string;
  
  // /**
  //  *  {string} resource The resource the client will use
  //  */
  // private resource: string;
  // TODO: Define constraints
  
  /**
   * @property {string} authorizationHeader The authorization header to pass the authentication token
   */
  protected authorizationHeader: string;
  
  constructor(registryUrl: string, authorizationHeader: string, registryToken: string) {
    this.registryUrl = registryUrl;
    this.authorizationHeader = authorizationHeader;
    this.registryToken = registryToken;
  }  
  
  /**
   * 
   * Constructs the headers for ant request
   * 
   * @returns {HeadersInit} The required headers of the request
   * 
   */
  getHeaders(): HeadersInit {
    const headers = {
      contentType: 'application/json',
      accept: 'application/json'
    };
    headers[this.authorizationHeader] = this.registryToken;
    
    return headers;
  }
  
  /**
   * 
   * Performs the data request for a specific action
   * 
   * @param action The action to perform against the registry
   * @param body The data to pass along with the action
   * 
   * @throws {ERROR} NotAvailableResource
   * @throws {RegistryError} An error from the registry
   * 
   * @returns {Promise<object>} The response of the action
   * 
   */
  async exec(resource: string, action: string, body: object): Promise<object> { 
    // TODO: define constraints
    // if (REGISTRY_AVAILABLE_RESOURCES.indexOf(action) < 0) {
    //   throw new NotAvailableResource(resource);
    // }
    const headers = this.getHeaders();
    try {
      const response = await axios.post(`${this.registryUrl}/${resource}/${action}`, body, { headers });
      return response.data;
    } catch (err) {
      throw new RegistryError(err);
    }
  }
  
  /**
   * 
   * Tests the connection with the registry service
   * 
   */
  async testConnection(): Promise<boolean> {
    const headers = this.getHeaders();
    
    try {
      await axios.post(`${this.registryUrl}/${TEST_RESOURCE}`, null, {
        headers: headers
      });
      return true;
    } catch(err)  {
      throw err;
    }
   }
}
