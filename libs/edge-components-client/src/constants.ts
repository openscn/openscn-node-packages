export const MANAGEMENT_TOKEN_HEADER_NAME = 'x-management-api-token';
export const EDGE_COMPONENTS_TOKEN_HEADER_NAME = 'x-ec-token';
export const TEST_RESOURCE = 'test';

export const MANAGEMENT_ENDPOINT_ACTIONS = [
  'attach-dependent',
  'create',
  'get-many',
  'get-one',
  'reset-uuid',
  'remove-dependent',
  'update'
];
