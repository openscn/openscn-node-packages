import { EdgeComponentsClient } from './edge-components-client';
import { MANAGEMENT_TOKEN_HEADER_NAME } from './constants';

export class EdgeComponentsManagementClient extends EdgeComponentsClient{

  constructor(registryUrl: string, registryToken: string) {
    super(registryUrl, MANAGEMENT_TOKEN_HEADER_NAME, registryToken);
  }

  async execEdge(action: string, data: object) {
    return this.exec('edge-components', action, data);
  }

  async execDependent(action: string, data: object) {
    return this.exec('dependent-components', action, data);
  }

  async attachDependent(data: object) {
    return this.execEdge('attach-dependent', data);
  }

  async detachDependent(data: object) {
    return this.execEdge('remove-dependent', data);
  }

  async createEdgeComponent(data: object) {
    return this.execEdge('create', data);
  }

  async getManyEdgeComponents(data: object) {
    return this.execEdge('get-many', data);
  }

  async getOneEdgeComponent(data: object) {
    return this.execEdge('get-one', data);
  }

  async updateEdgeComponent(data: object) {
    return this.execEdge('update', data);
  }

  async resetEdgeComponentUUID(data: object) {
    return this.execEdge('reset-uuid', data);
  }

  async createDependentComponent(data: object) {
    return this.execDependent('create', data);
  }

  async deleteDependentComponent(data: object) {
    return this.execDependent('delete', data);
  }
  
  async deleteEdgeComponent(data: object) {
    return this.execEdge('delete', data);
  }

  async getManyDependentComponents(data: object) {
    return this.execDependent('get-many', data);
  }

  async getOneDependentComponent(data: object) {
    return this.execDependent('get-one', data);
  }

  async updateDependentComponent(data: object) {
    return this.execDependent('update', data);
  }
}
